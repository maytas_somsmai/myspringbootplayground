package com.spring.playground.validator;

import com.spring.playground.anotation.ValidID;
import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

@Slf4j
public class IDValidator implements ConstraintValidator<ValidID, String> {

    private final Pattern patternMatchWhenContainOnlyNumberAndNotContainSpace = Pattern.compile("^[0-9]*$");

    private int maxLength;

    @Override
    public void initialize(ValidID validID) {
        maxLength = validID.maxLength();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        return value == null || (
                value.length() <= maxLength &&
                        patternMatchWhenContainOnlyNumberAndNotContainSpace.matcher(value).find()
        );
    }
}