package com.spring.playground.feature.kafka.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.component.kafka.KafkaConstants;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class KafkaProcessor implements Processor {
    @Override
    public void process(Exchange exchange) {
        String messageKey = "";
        if (exchange.getIn() != null) {
            Message message = exchange.getIn();
            Integer partitionId = (Integer) message
                    .getHeader(KafkaConstants.PARTITION);
            String topicName = (String) message
                    .getHeader(KafkaConstants.TOPIC);
            if (message.getHeader(KafkaConstants.KEY) != null)
                messageKey = (String) message
                        .getHeader(KafkaConstants.KEY);
            Object data = message.getBody();

            log.info(String.format("topic = %s | messageKey = %s | messageCode = %s",
                    topicName, messageKey, data));
        }
    }
}
