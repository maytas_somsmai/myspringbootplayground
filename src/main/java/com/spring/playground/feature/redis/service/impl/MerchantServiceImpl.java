package com.spring.playground.feature.redis.service.impl;

import com.querydsl.core.BooleanBuilder;
import com.spring.playground.entity.MerchantEntity;
import com.spring.playground.entity.QMerchantEntity;
import com.spring.playground.exception.ServiceException;
import com.spring.playground.exception.code.ErrorCode;
import com.spring.playground.feature.redis.service.MerchantService;
import com.spring.playground.repository.MerchantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.Optional;

@Service
public class MerchantServiceImpl implements MerchantService {

    private final MerchantRepository merchantRepository;

    @Autowired
    public MerchantServiceImpl(MerchantRepository merchantRepository) {
        this.merchantRepository = merchantRepository;
    }

    @Override
    public List<MerchantEntity> findAll(String id, String name) {
        BooleanBuilder predicate = new BooleanBuilder();
        if (id != null){
            predicate.and(QMerchantEntity.merchantEntity.id.eq(Long.parseLong(id)));
        }
        if (name != null){
            predicate.and(QMerchantEntity.merchantEntity.name.containsIgnoreCase(name));
        }
        return (List<MerchantEntity>) merchantRepository.findAll(predicate);
    }

    @Override
    public MerchantEntity findOne(String id) {
        final Optional<MerchantEntity> e = merchantRepository.findById(Long.parseLong(id));
        if (ObjectUtils.isEmpty(e)){
            throw new ServiceException(ErrorCode.MERCHANT_NOT_FOUND, "error.merchant.not_found", new Object[]{id});
        }
        return e.get();
    }

    @Override
    public MerchantEntity createNewMerchant(String name) {
        final MerchantEntity e = MerchantEntity.builder().name(name).build();
        return merchantRepository.save(e);
    }

    @Override
    public MerchantEntity updateExistMerchant(String id, String name) {
        Optional<MerchantEntity> e = merchantRepository.findById(Long.parseLong(id));
        if (ObjectUtils.isEmpty(e)){
            throw new ServiceException(ErrorCode.MERCHANT_NOT_FOUND, "error.merchant.not_found", new Object[]{id});
        }
        MerchantEntity result = e.get();
        result.setName(name);
        return merchantRepository.save(result);
    }
}
