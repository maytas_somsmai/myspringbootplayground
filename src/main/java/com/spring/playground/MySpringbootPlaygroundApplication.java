package com.spring.playground;

import com.spring.playground.config.AppConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@Slf4j
@SpringBootApplication
@EnableConfigurationProperties({ AppConfig.class })
public class MySpringbootPlaygroundApplication {

	public static void main(String[] args) {
		SpringApplication.run(MySpringbootPlaygroundApplication.class, args);
		log.info("Application start !");
	}

}

