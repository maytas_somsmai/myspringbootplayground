package com.spring.playground.repository;

import com.spring.playground.entity.MerchantEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface MerchantRepository extends JpaRepository<MerchantEntity, Long>, QuerydslPredicateExecutor<MerchantEntity> {
}
