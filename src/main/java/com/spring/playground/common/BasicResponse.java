package com.spring.playground.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BasicResponse<T> {
    private T data;
    private ErrorResponse status;

    public BasicResponse(T data) {
        this.data = data;
    }
}
