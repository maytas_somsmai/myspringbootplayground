package com.spring.playground.anotation;


import com.spring.playground.validator.IDValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(
        validatedBy = {IDValidator.class}
)
public @interface ValidID {

    int maxLength() default 10;

    String message() default "Should be numeric and max length is {maxLength} char";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
