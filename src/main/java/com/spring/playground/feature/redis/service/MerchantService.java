package com.spring.playground.feature.redis.service;

import com.spring.playground.entity.MerchantEntity;

import java.util.List;

public interface MerchantService {
    List<MerchantEntity> findAll(String id, String name);
    MerchantEntity findOne(String id);
    MerchantEntity createNewMerchant(String name);
    MerchantEntity updateExistMerchant(String id, String name);
}
