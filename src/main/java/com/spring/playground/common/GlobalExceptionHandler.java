package com.spring.playground.common;

import com.google.common.base.CaseFormat;
import com.spring.playground.exception.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

//For more information, check out this link :
// - https://sdqali.in/blog/2015/12/04/validating-requestparams-and-pathvariables-in-spring-mvc/
// - https://g00glen00b.be/validating-the-input-of-your-rest-api-with-spring/
@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    private final MessageSource msgSource;

    @Autowired
    public GlobalExceptionHandler(@Qualifier("messageSource") MessageSource msgSource) {
        this.msgSource = msgSource;
    }

    @ExceptionHandler(ServiceException.class)
    @ResponseBody
    public ResponseEntity<BasicResponse<Object>> handleException(ServiceException e) {
//        Locale currentLocale = LocaleContextHolder.getLocale();
        Locale currentLocale = new Locale("en", "US");
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setCode(e.getErrorCode());

        String message = "";
        if (!StringUtils.isEmpty(e.getMessageCode())) {
            message = msgSource.getMessage(e.getMessageCode(), e.getParams(), e.getMessageCode(), currentLocale);
        }
        errorResponse.setMessage(message);

        BasicResponse<Object> basicResponse = new BasicResponse<>();
        basicResponse.setStatus(errorResponse);
        return new ResponseEntity<>(basicResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    //for POST method
    public ResponseEntity<BasicResponse<Object>> handle(MethodArgumentNotValidException exception) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setCode("INVALID_PARAMETER");
        errorResponse.setMessage("Check errors below.");
        errorResponse.setErrors(exception.getBindingResult().getFieldErrors()
                .stream().map(fieldError -> ErrorResponse.ErrorV1Field.builder()
                        .code("INVALID_FORMAT")
                        .param(CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE,fieldError.getField()))
                        .message(fieldError.getDefaultMessage())
                        .build())
                .collect(Collectors.toList())
        );

        BasicResponse<Object> basicResponse = new BasicResponse<>();
        basicResponse.setStatus(errorResponse);
        return new ResponseEntity<>(basicResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    //for GET method
    public ResponseEntity<BasicResponse<Object>> handle(ConstraintViolationException exception) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setCode("INVALID_PARAMETER");
        errorResponse.setMessage("Check errors below.");
        errorResponse.setErrors(exception.getConstraintViolations()
                .stream().map(constraintViolation -> ErrorResponse.ErrorV1Field.builder()
                        .code("INVALID_FORMAT")
                        .param(
                                CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE,
                                        constraintViolation.getPropertyPath().toString().split("([.])")[1])
                        )
                        .message(constraintViolation.getMessage())
                        .build())
                .collect(Collectors.toList())
        );

        BasicResponse<Object> basicResponse = new BasicResponse<>();
        basicResponse.setStatus(errorResponse);
        return new ResponseEntity<>(basicResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    //for GET method
    public ResponseEntity<BasicResponse<Object>> handle(MissingServletRequestParameterException exception) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setCode("INVALID_PARAMETER");
        errorResponse.setMessage("Check errors below.");
        List<ErrorResponse.ErrorV1Field> errorV1Fields = new ArrayList<>();
        errorV1Fields.add(ErrorResponse.ErrorV1Field.builder()
                .code("MISSING_PARAMETER")
                .param(exception.getParameterName())
                .message("This parameter is require.")
                .build());
        errorResponse.setErrors(errorV1Fields);

        BasicResponse<Object> basicResponse = new BasicResponse<>();
        basicResponse.setStatus(errorResponse);
        return new ResponseEntity<>(basicResponse, HttpStatus.BAD_REQUEST);
    }
}