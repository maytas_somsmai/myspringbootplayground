package com.spring.playground.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DefaultMessage<T> {
    private final String id = GeneratorUtility.generateTransactionId();
    private T body;
}
