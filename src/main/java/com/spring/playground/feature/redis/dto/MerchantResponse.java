package com.spring.playground.feature.redis.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MerchantResponse {
    private String id;
    private String name;
}
