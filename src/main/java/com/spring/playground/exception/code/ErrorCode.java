package com.spring.playground.exception.code;

public enum ErrorCode {
    INVALID_PARAMETER,
    INVALID_REQUEST_BODY,

    MERCHANT_NOT_FOUND,

}
