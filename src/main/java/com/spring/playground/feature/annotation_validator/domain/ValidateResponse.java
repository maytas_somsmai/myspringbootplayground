package com.spring.playground.feature.annotation_validator.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ValidateResponse {
    private String paramOne;
    private String paramTwo;
}
