package com.spring.playground.feature.kafka.consumer;

import com.spring.playground.config.AppConfig;
import com.spring.playground.feature.kafka.service.KafkaProcessor;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class KafkaConsumer extends RouteBuilder {

    private final KafkaProcessor kafkaProcessor;

    private final AppConfig appConfig;

    @Autowired
    public KafkaConsumer(KafkaProcessor kafkaProcessor, AppConfig appConfig) {
        this.kafkaProcessor = kafkaProcessor;
        this.appConfig = appConfig;
    }

    @Override
    public void configure() {
        String server = "localhost:19092";
        String groupId = "groupId=testing";
        String autoOffsetReset = "autoOffsetReset=earliest";

        from(String.format("kafka:%s?%s&%s&consumersCount=1&topic=test1", server, groupId, autoOffsetReset))
                .routeId("KafkaConsumer1").autoStartup(appConfig.getKafkaConfig().isEnableConsumer())
                .process(kafkaProcessor);

        from(String.format("kafka:%s?%s&%s&consumersCount=1&topic=test2", server, groupId, autoOffsetReset))
                .routeId("KafkaConsumer2").autoStartup(appConfig.getKafkaConfig().isEnableConsumer())
                .process(kafkaProcessor);
    }

}
