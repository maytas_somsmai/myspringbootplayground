package com.spring.playground.feature.rabbitmq.consumer;

import com.spring.playground.config.AppConfig;
import com.spring.playground.feature.rabbitmq.service.RabbitProcessor;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RabbitConsumer extends RouteBuilder {

    private final AppConfig appConfig;

    private final RabbitProcessor rabbitProcessor;

    private final String host = "localhost:5672", username = "username=admin", password = "password=admin",
            consumerUrl1 = String.format("rabbitmq://%s?%s&%s&queue=queue.1&declare=false", host, username, password),
            consumerUrl2 = String.format("rabbitmq://%s?%s&%s&queue=queue.2&declare=false", host, username, password);

    @Autowired
    public RabbitConsumer(RabbitProcessor rabbitProcessor, AppConfig appConfig) {
        this.rabbitProcessor = rabbitProcessor;
        this.appConfig = appConfig;
    }

    @Override
    public void configure() {
        from(consumerUrl1)
                .routeId("RabbitConsumer1").autoStartup(appConfig.getRabbitConfig().isEnableConsumer())
                .process(rabbitProcessor);

        from(consumerUrl2)
                .routeId("RabbitConsumer2").autoStartup(appConfig.getRabbitConfig().isEnableConsumer())
                .process(rabbitProcessor);
    }

}
