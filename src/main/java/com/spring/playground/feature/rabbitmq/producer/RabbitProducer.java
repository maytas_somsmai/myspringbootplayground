package com.spring.playground.feature.rabbitmq.producer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spring.playground.common.DefaultMessage;
import com.spring.playground.config.AppConfig;
import com.spring.playground.feature.rabbitmq.domain.RabbitMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.rabbitmq.RabbitMQConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RabbitProducer extends RouteBuilder {

    private final String host = "localhost:5672", username = "username=admin", password = "password=admin",
            exchange1 = "test.exchange.1",
            routeUrl1 = String.format("rabbitmq://%s/%s?%s&%s&declare=false&skipQueueDeclare=true", host, exchange1, username, password);

    private final ObjectMapper mapper;

    private final AppConfig appConfig;

    @Autowired
    public RabbitProducer(ObjectMapper mapper, AppConfig appConfig) {
        this.mapper = mapper;
        this.appConfig = appConfig;
    }

    @Override
    public void configure() {
        if (appConfig.getRabbitConfig().isEnableProducer()) {
            from("jetty:http://localhost:12345/rabbit")
                    .routeId("rabbitProducer")
                    .process(
                            exchange -> {
                                final DefaultMessage message = DefaultMessage.builder().body(
                                        exchange.getIn().getBody() != null
                                                ? mapper.readValue(exchange.getIn().getBody(String.class), RabbitMessage.class)
                                                : "TEST JETTY"
                                ).build();
                                exchange.getIn().setBody(mapper.writeValueAsString(message), String.class);
                                exchange.getIn().setHeader(RabbitMQConstants.ROUTING_KEY, exchange.getIn().getHeader("routing_key"));
                                exchange.getIn().setHeader(RabbitMQConstants.MESSAGE_ID, message.getId());
                                exchange.getIn().setHeader(RabbitMQConstants.CONTENT_ENCODING, "utf-8");
                                exchange.getIn().setHeader(RabbitMQConstants.CONTENT_TYPE, "application/json");
                            }).to(routeUrl1).log("SENDING MESSAGE!!!");
        }
    }

}
