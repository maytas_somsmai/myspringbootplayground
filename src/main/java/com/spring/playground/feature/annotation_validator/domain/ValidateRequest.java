package com.spring.playground.feature.annotation_validator.domain;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.spring.playground.anotation.ValidID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ValidateRequest {
    @ValidID(maxLength = 1, message = "Should be numeric and max length is 1 char")
    private String paramOne;
    @NotNull @NotBlank
    private String paramTwo;
}
