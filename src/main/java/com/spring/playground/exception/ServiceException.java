package com.spring.playground.exception;

import com.spring.playground.exception.code.ErrorCode;
import io.netty.util.internal.EmptyArrays;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;

@Data
@EqualsAndHashCode(callSuper = true)
public class ServiceException extends RuntimeException{

    protected final HttpStatus httpStatus;
    protected final String errorCode;
    protected final String messageCode;
    protected final transient Object[] params;

    public ServiceException(HttpStatus httpStatus, ErrorCode errorCode, String messageCode, Object... params) {
        this.httpStatus = httpStatus;
        this.errorCode = errorCode.name();
        this.messageCode = messageCode;
        this.params = params;
    }

    public ServiceException(ErrorCode errorCode, String messageCode, Object[] params) {
        this(HttpStatus.BAD_REQUEST, errorCode, messageCode, params);
    }

    public ServiceException(ErrorCode errorCode, String messageCode) {
        this(HttpStatus.BAD_REQUEST, errorCode, messageCode, EmptyArrays.EMPTY_OBJECTS);
    }
}
