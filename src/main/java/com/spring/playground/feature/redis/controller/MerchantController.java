package com.spring.playground.feature.redis.controller;

import com.spring.playground.common.BasicResponse;
import com.spring.playground.entity.MerchantEntity;
import com.spring.playground.feature.redis.dto.MerchantResponse;
import com.spring.playground.feature.redis.service.MerchantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/v1/merchant")
public class MerchantController {
    private final MerchantService merchantService;

    @Autowired
    public MerchantController(MerchantService merchantService) {
        this.merchantService = merchantService;
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<BasicResponse<List<MerchantResponse>>> getMerchantList(
            @RequestParam(required = false) String id,
            @RequestParam(required = false) String name
    ) {
        final List<MerchantEntity> e = merchantService.findAll(id, name);
        BasicResponse response = new BasicResponse();
        List<MerchantResponse> responses = new ArrayList<>();
        if (!ObjectUtils.isEmpty(e)){
            e.forEach( entity -> responses.add(this.mapMerchantDetail(entity)));
        }
        response.setData(responses);
        return ResponseEntity.ok().body(response);
    }

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<BasicResponse<MerchantResponse>> createNewMerchant(@RequestParam String name) {
        final MerchantEntity e = merchantService.createNewMerchant(name);
        BasicResponse response = new BasicResponse();
        response.setData(ObjectUtils.isEmpty(e) ? null : mapMerchantDetail(e));
        return ResponseEntity.ok().body(response);
    }

    @RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<BasicResponse<MerchantResponse>> updateExistMerchant(@RequestParam String id, @RequestParam String name) {
        final MerchantEntity e = merchantService.updateExistMerchant(id, name);
        BasicResponse response = new BasicResponse();
        response.setData(ObjectUtils.isEmpty(e) ? null : mapMerchantDetail(e));
        return ResponseEntity.ok().body(response);
    }

    private MerchantResponse mapMerchantDetail(MerchantEntity e){
        return MerchantResponse.builder().id(e.getId().toString()).name(e.getName()).build();
    }
}
