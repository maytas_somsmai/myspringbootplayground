package com.spring.playground.feature.kafka.producer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spring.playground.common.DefaultMessage;
import com.spring.playground.config.AppConfig;
import com.spring.playground.feature.kafka.domain.KafkaMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.kafka.KafkaConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class KafkaProducer extends RouteBuilder {

    private final ObjectMapper mapper;

    private final AppConfig appConfig;

    @Autowired
    public KafkaProducer(ObjectMapper mapper, AppConfig appConfig) {
        this.mapper = mapper;
        this.appConfig = appConfig;
    }

    @Override
    public void configure() {

        if (appConfig.getKafkaConfig().isEnableProducer()) {

            String server = "localhost:19092";

            from("jetty:http://localhost:12345/kafka")
                    .routeId("kafkaProducer")
                    .process(
                            exchange -> {
                                final DefaultMessage message = DefaultMessage.builder().body(
                                        exchange.getIn().getBody() != null
                                                ? mapper.readValue(exchange.getIn().getBody(String.class), KafkaMessage.class)
                                                : "TEST JETTY"
                                ).build();
                                exchange.getIn().setBody(mapper.writeValueAsString(message), String.class);
                                exchange.getIn().setHeader(KafkaConstants.PARTITION_KEY, 0);
                                exchange.getIn().setHeader(KafkaConstants.KEY, "1");
                                exchange.getIn().setHeader(KafkaConstants.TOPIC, exchange.getIn().getHeader("topic"));
                            }).to(String.format("kafka:%s", server)).log("SENDING MESSAGE!!!");
        }
    }
}
