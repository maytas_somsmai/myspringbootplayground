package com.spring.playground.feature.annotation_validator.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spring.playground.common.BasicResponse;
import com.spring.playground.feature.annotation_validator.domain.ValidateRequest;
import com.spring.playground.feature.annotation_validator.domain.ValidateResponse;
import com.spring.playground.feature.annotation_validator.service.MapValue;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest({AnnotationValidator.class, MapValue.class})
public class AnnotationValidatorTest {

    @Autowired
    private MockMvc mockMvc;

    private final ObjectMapper mapper = new ObjectMapper();

    @Test
    public void callGetWithAllValidParams() throws Exception {
        mockMvc.perform(get("/v1/get")
                .param("param_one","12345")
                .param("param_two","1"))
                .andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void callGetWithAllInvalidParams() throws Exception {

        Map<String,String[]> expectResult = new HashMap<String, String[]>(){{
            put("param_one", new String[]{"INVALID_FORMAT", "Should be numeric and max length is 5 char"});
            put("param_two", new String[]{"INVALID_FORMAT", "Should be numeric and max length is 1 char"});
        }};

        MvcResult result = mockMvc.perform(get("/v1/get")
                .param("param_one","test")
                .param("param_two","test"))
                .andDo(print()).andExpect(status().isBadRequest()).andReturn();

        BasicResponse<ValidateResponse> response = mapper.readValue(result.getResponse().getContentAsString(), BasicResponse.class);

        response.getStatus().getErrors().forEach( e -> {
            Assert.assertThat(e.getCode(), is(expectResult.get(e.getParam())[0]));
            Assert.assertThat(e.getMessage(), is(expectResult.get(e.getParam())[1]));
        });

        if (expectResult.size() != response.getStatus().getErrors().size()){
            Assert.fail("There have other or missing errors that not expect on this test case : " + mapper.writeValueAsString(response));
        }
    }

    @Test
    public void callPostWithAllValidParams() throws Exception {
        mockMvc.perform(post("/v1/post")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsString(ValidateRequest.builder().paramOne("1").paramTwo("12345").build())))
                .andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void callPostWithAllInvalidParams() throws Exception {

        Map<String,String[]> expectResult = new HashMap<String, String[]>(){{
            put("param_one", new String[]{"INVALID_FORMAT", "Should be numeric and max length is 1 char"});
            put("param_two", new String[]{"INVALID_FORMAT", "must not be blank"});
        }};

        MvcResult result = mockMvc.perform(post("/v1/post")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapper.writeValueAsString(ValidateRequest.builder().paramOne("test").paramTwo("").build())))
                .andDo(print()).andExpect(status().isBadRequest()).andReturn();

        BasicResponse<ValidateResponse> response = mapper.readValue(result.getResponse().getContentAsString(), BasicResponse.class);

        response.getStatus().getErrors().forEach( e -> {
            Assert.assertThat(e.getCode(), is(expectResult.get(e.getParam())[0]));
            Assert.assertThat(e.getMessage(), is(expectResult.get(e.getParam())[1]));
        });

        if (expectResult.size() != response.getStatus().getErrors().size()){
            Assert.fail("There have other or missing errors that not expect on this test case : " + mapper.writeValueAsString(response));
        }
    }
}