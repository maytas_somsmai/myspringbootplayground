package com.spring.playground.feature.rabbitmq.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.component.rabbitmq.RabbitMQConstants;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RabbitProcessor implements Processor {
    @Override
    public void process(Exchange exchange) {
        if (exchange.getIn() != null) {
            Message message = exchange.getIn();
            String data = message.getBody(String.class);
            log.info(String.format("fromConsumer = %s | messageId = %s | routingKey = %s | messageCode = %s",
                    exchange.getFromRouteId(),
                    exchange.getIn().getHeader(RabbitMQConstants.MESSAGE_ID),
                    exchange.getIn().getHeader(RabbitMQConstants.ROUTING_KEY),
                    data));
        }
    }
}
