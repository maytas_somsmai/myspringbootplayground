![Alt text](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Git-logo.svg/2000px-Git-logo.svg.png)
# My-SpringBoot-Playground
## SpringBoot project
This is Maven project. To test SpringBoot feature.

### All feature in this project
1. Apache Camel-rabbitmq
2. Apache Camel-kafka
3. Apache Camel-jetty
4. Validation Annotation
5. Redis cache -> to do
6. Spring AOP for evict cache -> to do

### Prerequisites
1. [Lombok](https://projectlombok.org/)</u> - Install Lombok in your favorite IDE.

### Running
```shell
$ mvn spring-boot:run
```
```shell
# skip unit test
$ mvn spring-boot:run -DskipTests
```
___


## Apache kafka
To learning about Apache Kafka.

### Prerequisites
What things you need to install the software and how to install them.

1. [Docker](http://www.docker.com)</u> - For running local database.

2. [HomeBrew](https://brew.sh/)</u> - For easy install Kafkacat.

3. [Kafkacat](https://docs.confluent.io/current/app-development/kafkacat-usage.html)</u> - Kafka command-line interface.

4. Please remember to add kafka-1, kafka-2 and kafka-3 hosts to the client /etc/hosts file

```shell
127.0.0.1     kafka-1 kafka-2 kafka-3
```

### Running
```shell
# Run Apache Kafka cluster
$ docker-compose up -d
```

### Test with Kafkacat
```shell
# To list all available brokers in the cluster
$ kafkacat -L -b kafka-1:19092
```
```shell
# To send message
$ kafkacat -P -b kafka-1:19092 -t helloworld_topic
```
```shell
# To receive message
$ kafkacat -C -b kafka-2:29092 -t helloworld_topic
```
___


[Markdown-Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)</u> - Markdown-Cheatsheet syntax.