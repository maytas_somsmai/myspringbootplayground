package com.spring.playground.feature.annotation_validator.controller;

import com.spring.playground.anotation.ValidID;
import com.spring.playground.common.BasicResponse;
import com.spring.playground.feature.annotation_validator.domain.ValidateRequest;
import com.spring.playground.feature.annotation_validator.domain.ValidateResponse;
import com.spring.playground.feature.annotation_validator.service.MapValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@RestController
@Validated
public class AnnotationValidator {

    private final MapValue mapValue;

    @Autowired
    public AnnotationValidator(MapValue mapValue) {
        this.mapValue = mapValue;
    }

    @GetMapping("/v1/get")
    public BasicResponse<ValidateResponse> get(
            @RequestParam(name = "param_one") @NotBlank @ValidID(maxLength = 5, message = "Should be numeric and max length is 5 char") String paramOne,
            @RequestParam(name = "param_two", required = false) @ValidID(maxLength = 1, message = "Should be numeric and max length is 1 char") String paramTwo
    ){
        return new BasicResponse<>(mapValue.mapvalueToResponse(paramOne, paramTwo));
    }

    @PostMapping("/v1/post")
    public BasicResponse<ValidateResponse> post(@RequestBody @Valid ValidateRequest request){
        return new BasicResponse<>(mapValue.mapvalueToResponse(request.getParamOne(), request.getParamTwo()));
    }
}
