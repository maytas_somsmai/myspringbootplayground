package com.spring.playground.feature.annotation_validator.service;

import com.spring.playground.feature.annotation_validator.domain.ValidateResponse;
import org.springframework.stereotype.Service;

@Service
public class MapValue {
    public ValidateResponse mapvalueToResponse(String... params) {
        return ValidateResponse.builder().paramOne(params[0]).paramTwo(params[1]).build();
    }
}
