package com.spring.playground.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "app")
public class AppConfig {

    private KafkaConfig kafkaConfig;
    private RabbitConfig rabbitConfig;

    @Data
    public static class KafkaConfig{
        private boolean enableProducer;
        private boolean enableConsumer;
    }

    @Data
    public static class RabbitConfig{
        private boolean enableProducer;
        private boolean enableConsumer;
    }
}
